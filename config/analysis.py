# coding: utf-8
# flake8: noqa

"""
Definition of an analysis example
"""

import scinum as sn
import utils.aci as aci
import six
import copy

#
# analysis and config
#
# import config.Run2_pp_13TeV_2017 as run_2017
from config.analysis import analysis
from config.util import PrepareConfig

# get = od.Process.get_instance


def rgb(r, g, b):
    return (r, g, b)


# create the analysis
config_2017 = cfg = analysis.copy(name="example_analysis")
get = analysis.processes.get
#analysis = od.Analysis("example_analysis", 1)

# setup the config for the 2017 campaign
# we pass no name, so the config will have the same name as the campaign
# config_2017 = cfg = run_2017.default_config(analysis=analysis)

cfg.aux["stat_model"] = "example_model"
print("cfg in analysis.py:", cfg)

#
# process groups
#
cfg.aux["process_groups"] = {
        "default": [
            # "data_dl",
            "dy",
            "tt",
            "ttV",
            "ttVH",
            "ttVV",  # one file missing?
        ],
        "plotting": [
            # "data_dl",
            "tt",
            "dy",
            "ttV",
            "ttVH",
            "ttVV",
            # "plot_other",
        ],
    }
cfg.aux["signal_prcess"] = "dy"

cfg.aux["btag_sf_shifts"] = [
        "lf",
        "lfstats1",
        "lfstats2",
        "hf",
        "hfstats1",
        "hfstats2",
        "cferr1",
        "cferr2",
    ]

processes = [
    {
        "name": "plot_other",
        "idx": 999992,
        "color": rgb(0, 183, 153),
        "label": r"Other",
        "processes": [
            get("ttV"),
            get("ttVV"),
            get("ttVH"),
        ],
    },
#     {
#         "name": "ttX",
#         "idx": 999995,
#         "label": r"ttX",
#         "processes": [get("ttV"), get("ttVV"), get("ttVH")],
#     },
]

specific_processes = []
for process in processes:
    specific_processes.append(
        aci.Process(
            name=process["name"],
            id=process["idx"],
            color=process.get("color", rgb(55, 55, 55)),
            label=process["label"],
            processes=process["processes"],
        )
    )
# add all processes now
# config_2017.processes.extend(specific_processes)


# channels to use
channels = ["mumu", "ee"]
# remove all channels from cfg which are not in `channels`

# from IPython import embed;embed()
PrepareConfig(
    config_2017,
    # channels=channels,
    # defines which datasets we will use
    processes=[
        "dy_lep_10To50",
        "dy_lep_nj",
        "tt",
        "ttV",
        "ttVH",
        "ttVV",
        "data_ee",
        "data_mumu",
    ],
    ignore_datasets=[],  # "TTWW", "data_B_ee"
)


from example_analysis.config.categories import setup_categories

setup_categories(config_2017)

from example_analysis.config.variables import setup_variables

setup_variables(config_2017)
