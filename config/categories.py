# coding: utf-8
# flake8: noqa
import utils.aci as aci

categories = [
    {
        "name": "ee_1b",
        "label": "ee 1 btagged jets",
        "label_short": "ee 1b",
        "aux": {"fit": False, "fit_variable": "mll"},
    },
    {
        "name": "mumu_1b",
        "label": "mumu 1 btagged jets",
        "label_short": "mumu 1b",
        "aux": {"fit": False, "fit_variable": "mll"},
    },
    {
        "name": "ee_0b",
        "label": "ee 1 btagged jets",
        "label_short": "ee 0b",
        "aux": {"fit": False, "fit_variable": "mll"},
    },
    {
        "name": "mumu_0b",
        "label": "mumu 1 btagged jets",
        "label_short": "mumu 0b",
        "aux": {"fit": False, "fit_variable": "mll"},
    },
]


def setup_categories(cfg):

    # for category in categories:
    #     cfg.add_category(
    #         category["name"],
    #         label=category["label"],
    #         label_short=category["label_short"],
    #         aux=category["aux"],
    #     )

    cfg.aux["collate_cats"] = {
        ("_resolved_1b", "_resolved_2b", "_boosted"): ("_incl"),
    }

    get = cfg.processes.get

    modes = {
        "boosted_1b": "boost.",
        "resolved_1b": "res. = 1b",
        "resolved_2b": "res. ≥ 2b",
    }
    channel = {"ee": "ee", "mumu": r"μμ"}
    for ch, chtex in channel.items():
        for mode, modetex in modes.items():
            nbase = f"{ch}_{mode}"
            lbase = f"{chtex} {modetex}"
            # sbase = f"${chtex}$ ${modetex}$"
            cfg.categories.add(
                aci.Category(name=nbase, label=lbase, aux=dict(fit_variable="jet1_pt"))
            )
