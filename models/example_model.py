# coding: utf-8

import os
import numpy as np
import hist
import example_analysis.config.analysis as example_config 
from utils.datacard import Datacard, RDict
from utils.util import reluncs, wquant
from enum import IntEnum, unique
from rich.console import Console
import rich.table


# @unique
# class Processes(IntEnum):

# # Signal Processes
# # They have to be <= 0
# WH = -2
# ZH_bbll = -1
# ggZH_bbll = 0

# # Background Processes
# # They have to be > 0

Processes = {}
all_processes = example_config.cfg.aux["process_groups"]["default"].copy()

Processes[example_config.cfg.aux["signal_process"]] = -1

for index, process in enumerate(all_processes, start=1):
    Processes[process] = index


class DataCardLogger(Datacard):

    uncertainty_dict = {}

    def __init__(self, analysis, category, variable, hists, config_inst, year):
        super().__init__(analysis, category, variable, hists, config_inst, year)

    def add_systematics(self, names, type, processes, **kwargs):
        if isinstance(names, dict):
            for name, values in names.items():
                self.uncertainty_dict[name] = {
                    "value": values,
                    "type": type,
                    "processes": processes,
                }
        if isinstance(names, list):
            for name in names:
                self.uncertainty_dict[name] = {
                    "value": "",
                    "type": type,
                    "processes": processes,
                }
        elif isinstance(names, str):
            self.uncertainty_dict[names] = {
                "value": "",
                "type": type,
                "processes": processes,
            }
        super().add_systematics(names, type, processes, **kwargs)

    def dump_uncertainty_table(self, target=None, console_output=True):
        table = rich.table.Table(title="Variables")
        table.add_column("Name")
        table.add_column("type")
        table.add_column("value")
        table.add_column("processes")
        for name, val in sorted(self.uncertainty_dict.items()):
            if isinstance(val["value"], (list, set)):
                value = ", ".join(map(str, list(val["value"])))
            else:
                value = str(val["value"])
            table.add_row(name, value, val["type"], " ".join(list(val["processes"])))

        if target is not None:
            with open(target, "wt") as report_file:
                console = Console(file=report_file)
                console.print(table)
        if console_output:
            console = Console()
            console.print(table)


class StatModel(DataCardLogger):
    _processes = Processes

    @property
    def custom_lines(self):
        return [
            f"{self.bin} autoMCStats 10",
        ]

    # hook for renamings
    # rename processes
    # @property
    # def rprocesses(self):
    #     return RDict(os.path.abspath("bbww_dl/models/processes.txt"))
    #
    # # rename nuisances
    # @property
    # def rnuisances(self):
    #     return RDict(os.path.abspath(f"bbww_dl/models/systematics_{self.year}.txt"))

    @classmethod
    def requires(cls, task):
        from tasks.group import Rebin

        return Rebin.req(task, process_group=list(Processes.keys()))  # + ["data"]

    def build_systematics(self):
        # lumi https://gitlab.cern.ch/hh/naming-conventions#luminosity

        lumi = {
            2017: {
                "CMS_lumi_13TeV_2017": 1.020,
            },
        }

        self.add_systematics(
            names=lumi[int(self.year)],
            type="lnN",
            processes=self.processes,
        )

        # process xsec uncertainties
        # https://gitlab.cern.ch/hh/naming-conventions#theory-uncertainties
        self.add_systematics(
            names={
                "m_top_unc_ggHH": 1.026,
                "alpha_s_ggHH": 1.021,
                "pdf_ggHH": 1.021,
                # scale uncertainty will be added by PhysicsModel
                "QCDscale_ggHH": (0.950, 1.022),
            },
            type="lnN",
            processes=[p for p in self.processes if p.startswith("ggHH_")],
        )
        self.add_systematics(
            names={
                "pdf_qqHH": 1.021,
                # scale uncertainty will be added by PhysicsModel
                "QCDscale_qqHH": (0.9996, 1.0003),
            },
            type="lnN",
            processes=[p for p in self.processes if p.startswith("qqHH_")],
        )

        # tt
        self.add_systematics(
            names={
                "m_top_unc_tt": reluncs("tt", uncs="mtop"),
                "pdf_tt": reluncs("tt", uncs="pdf"),
                "QCDscale_tt": reluncs("tt", uncs="scale"),
                "CMS_bbWW_TT_norm": 1.3,
            },
            type="lnN",
            processes=["tt"],
        )
        # dy
        self.add_systematics(
            names={
                "integration_dy": reluncs("dy_lep_50ToInf", uncs="integration"),
                "pdf_dy": reluncs("dy_lep_50ToInf", uncs="pdf"),
                "QCDscale_dy": reluncs("dy_lep_50ToInf", uncs="scale"),
            },
            type="lnN",
            processes=["dy"],
        )
        self.add_systematics(
            names={
                "pdf_st": reluncs("st", uncs="pdf"),
                "QCDscale_st": reluncs("st", uncs="scale"),
            },
            type="lnN",
            processes=["st"],
        )
        # add shape uncs
        # misc
        self.add_systematics(
            names="top_pT_reweighting",
            type="shape",
            strength=1.0,
            processes=["tt"],
        )

        common_shape_systs = [
            "pileup",
        ]
        if self.year in ("2016", "2017"):
            common_shape_systs += ["l1_ecal_prefiring"]
        self.add_systematics(
            names=common_shape_systs,
            type="shape",
            strength=1.0,
            processes=self.processes,
        )

        btagshifts = [
            "btagWeight_hf",
            "btagWeight_hfstats1",
            "btagWeight_hfstats2",
            "btagWeight_lf",
            "btagWeight_lfstats1",
            "btagWeight_lfstats2",
            "btagWeight_subjet",
        ]

        self.add_systematics(
         names=self.config_inst.aux["jes_sources"] + ["jer", "UnclustEn"],
         type="shape", strength=1.0,
         processes=self.processes,
         suffix=("_up", "_down"),
        )

        # lepton efficiencies
        self.add_systematics(
            names="electron*",
            type="shape",
            strength=1.0,
            processes=self.processes,
        )
        self.add_systematics(
         names="muon*",
         type="shape",
         strength=1.0,
         processes=self.processes,
        )
        # trigger
        self.add_systematics(
         names="trigger*",
         type="shape",
         strength=1.0,
         processes=self.processes,
        )